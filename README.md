## 学习springcloud

- 学习Rest[微服务构建](./springcloud-consumer-dept-80/src/main)
- 学习eureka [服务注册与发现](./springcloud-eureka-7001/src/main)
- 学习eureka级群搭建 7001 7002 7003
- 学习 eureka + [ribbon负载均衡](./springcloud-consumer-dept-ribbon-80/src/main) --> 面向服务（服务名调用）
- Feign [负载均衡](./springcloud-consumer-dept-feign-80/src/main)  ---> 面向接口和注解
- Hystrix [服务熔断](./springcloud-provider-dept-hystrix-8001/src/main) 
    - 服务熔断：一般是某个服务故障或者异常引起，类似现实世界中的 “保险丝” ， 当某个异常条件被触发，
    直接熔断整个服务，而不是一直等到此服务超时！
- Hystrix [服务降级]()
    - 服务降级：所谓降级，一般是从整体负荷考虑，就是当某个服务熔断之后，服务器将不再被调用，此时
      客户端可以自己准备一个本地的fallback回调，返回一个缺省值。这样做，虽然服务水平下降，但好歹可
      用，比直接挂掉要强。
- Hystrix Dashboard [服务监控](./springcloud-consumer-hystrix-dashboard-9001/src/main)
- Zuul [网关](./springcloud-zuul-gateway-9527) 路由。过滤功能
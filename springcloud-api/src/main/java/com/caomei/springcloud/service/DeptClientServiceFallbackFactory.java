package com.caomei.springcloud.service;

import com.caomei.springcloud.pojo.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 服务降级
 * 所谓降级，一般是从整体负荷考虑，就是当某个服务熔断之后，服务器将不再被调用，此时
 *       客户端可以自己准备一个本地的fallback回调，返回一个缺省值。这样做，虽然服务水平下降，但好歹可
 *       用，比直接挂掉要强。
 *  当服务挂掉时  会调用
 */
@Component // 放到容器中
public class DeptClientServiceFallbackFactory implements FallbackFactory<DeptClientService> {

    public DeptClientService create(Throwable throwable) {
        return new DeptClientService() {
            public Dept queryById(Long id) {
                return new Dept().setDeptno(id)
                        .setDname("该id："+id+"没有对应的信息，Consumer客户端提供的降级信息，此刻服务Provider已经关闭")
                                        .setDb_source("no this database in MySQL");
            }

            public List<Dept> queryAll() {
                return null;
            }

            public boolean addDept(Dept dept) {
                return false;
            }
        };
    }
}

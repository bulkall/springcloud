package com.caomei.springcloud.config;


import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigBean {

    @Bean
    @LoadBalanced // Spring cloud Ribbon 是基于Netflix Ribbon 实现的一套客户端负载均衡的工具
    public RestTemplate getResTemplate(){
        return new RestTemplate();
    }
}

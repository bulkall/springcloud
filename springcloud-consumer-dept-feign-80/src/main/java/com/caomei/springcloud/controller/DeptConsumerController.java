package com.caomei.springcloud.controller;

import com.caomei.springcloud.pojo.Dept;
import com.caomei.springcloud.service.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DeptConsumerController {
    // 这里是消费者，不应该有service 层，而是直接调用生产者提供的

    @Autowired
    private DeptClientService service = null;

    @RequestMapping("consumer/dept/add")
    public Boolean add(Dept dept) {
        return service.addDept(dept);
    }

    @RequestMapping("/consumer/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        System.out.println(service.queryById(id));
        return service.queryById(id);
    }

    @RequestMapping("/consumer/dept/list")
    public List list() {
        return service.queryAll();
    }
}

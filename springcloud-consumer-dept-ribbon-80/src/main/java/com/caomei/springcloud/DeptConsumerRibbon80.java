package com.caomei.springcloud;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient // 开启
public class DeptConsumerRibbon80 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumerRibbon80.class,args);
    }
}

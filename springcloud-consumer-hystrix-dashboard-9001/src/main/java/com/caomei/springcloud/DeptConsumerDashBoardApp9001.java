package com.caomei.springcloud;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableHystrixDashboard // 开启
public class DeptConsumerDashBoardApp9001 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumerDashBoardApp9001.class,args);
    }
}

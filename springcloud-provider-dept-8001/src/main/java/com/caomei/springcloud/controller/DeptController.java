package com.caomei.springcloud.controller;

import com.caomei.springcloud.pojo.Dept;
import com.caomei.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dept")
public class DeptController {
    @Autowired
    private DeptService service;

    @Autowired
    private DiscoveryClient client;
    @PostMapping("/add")
    public boolean addDept(@RequestBody Dept dept) {
        return service.addDept(dept);
    }

    @GetMapping("/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        return service.queryById(id);
    }

        @GetMapping("/list")
    public List<Dept> queryAll() {
        return service.queryAll();
    }

    /**
     * 对于注册进eureka里面的微服务，可以通过服务发现来获得该服务的信息。对外暴露
     * @return
     */
    @GetMapping("/discovery")
    public Object discovery(){
        List<String> list = client.getServices();
        System.out.println("client.getService====>" + list);

        // 得到具体一个服务
        List<ServiceInstance> instances = client.getInstances("springcloud-provider-dept");
        for (ServiceInstance serviceInstance : instances){
            System.out.println(
                    serviceInstance.getServiceId() + "\t" +
                    serviceInstance.getHost() + "\t" +
                    serviceInstance.getPort() + "\t" +
                    serviceInstance.getUri() + "\t"
            );
        }
        return this.client;
    }
}

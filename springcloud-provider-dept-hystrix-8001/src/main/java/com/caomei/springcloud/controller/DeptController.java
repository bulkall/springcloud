package com.caomei.springcloud.controller;

import com.caomei.springcloud.pojo.Dept;
import com.caomei.springcloud.service.DeptService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dept")
public class DeptController {
    @Autowired
    private DeptService service;

    //一旦调用服务方法失败并抛出了错误信息后
    // 会自动调用HystrixCommand标注好的fallbackMethod调用类中指定方法
    @GetMapping("/get/{id}")
    @HystrixCommand(fallbackMethod = "processHystrixGet") // 服务熔断
    public Dept get(@PathVariable("id") Long id) {
        System.out.println("------->"+id);
        Dept dept = service.queryById(id);
        if (dept == null){
            throw new RuntimeException("暂无" + id + "信息"); // 必须是抛出异常之后，hystrixCommand才起作用
        }
        return dept;
    }

    public Dept processHystrixGet(@PathVariable("id") Long id) {
        return new Dept().setDeptno(id)
                .setDname("没有对应 -》" + id + " 的信息， --- @hystrixCommand")
                .setDb_source("无");
    }


}

package com.caomei.springcloud.dao;

import com.caomei.springcloud.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DeptDao {
    public boolean addDept(Dept dept); //添加一个部门
    public Dept queryById(Long id); //根据id查询部门
    public List<Dept> queryAll(); //查询所有部门
}

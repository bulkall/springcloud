package com.caomei.springcloud.service;

import com.caomei.springcloud.pojo.Dept;

import java.util.List;


public interface DeptService {
    public boolean addDept(Dept dept); //添加一个部门
    public Dept queryById(Long id); //根据id查询部门
    public List<Dept> queryAll(); //查询所有部门
}
